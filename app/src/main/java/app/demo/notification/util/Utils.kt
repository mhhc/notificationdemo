package app.demo.notification.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.net.Uri
import android.webkit.URLUtil
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import app.demo.notification.R
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

/*
 * Opens url in custom tab inside the app
 */
fun openCustomBrowser(context: Context, url: String) {
    if (!URLUtil.isValidUrl(url)) {
        "https://www.google.com/search?q=${URLEncoder.encode(url, StandardCharsets.UTF_8.name())}"
    }
    val builder = CustomTabsIntent.Builder()
    val customTabsIntent = builder.build()
    try {
        customTabsIntent.launchUrl(context, Uri.parse(url))
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(context, context.resources.getString(R.string.error_open_in_browser), Toast.LENGTH_SHORT).show()
    }
}