package app.demo.notification.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View
import android.webkit.URLUtil
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import app.demo.notification.MainActivity
import app.demo.notification.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.lang.Integer.parseInt
import java.text.SimpleDateFormat
import java.util.*

/*
 * Helper class to create notifications, manage notification channels, etc.
 */
class NotificationHelper(private val activityContext: Context) {

    /**
     * notificationId is a unique int for each notification that you must define.
     * Remember to save the notification ID passed to NotificationManagerCompat.notify()
     * for later updating or removing the notification.
     */
    private var notificationId: Int = createUniqueId()
    private var notificationManager: NotificationManagerCompat

    init {
        notificationId = createUniqueId()
        notificationManager = NotificationManagerCompat.from(activityContext);
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel
        // https://developer.android.com/training/notify-user/channels
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = activityContext.getString(R.string.default_notification_channel_name)
            val descriptionText = activityContext.getString(R.string.default_notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(DEFAULT_CHANNEL_ID, name, importance)
            channel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviours after this
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun sendNotification(
        contentIconRes: Int,
        contentTitle: String?,
        contentText: String?
    ) {
        // Create an explicit intent to handle notification
        val intent = Intent(activityContext, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        intent.putExtra(EXTRA_NOTIFICATION_CONTENT_TEXT, contentText)

        val pendingIntent: PendingIntent = PendingIntent.getActivity(
            activityContext,
            createUniqueId(),
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT // https://developer.android.com/reference/android/app/PendingIntent#constants
        )

        // channel ID is required for compatibility with Android 8.0 (API level 26) and higher, but is ignored by older versions.
        // https://developer.android.com/training/notify-user/build-notification
        val builder = NotificationCompat.Builder(activityContext, DEFAULT_CHANNEL_ID)
            .setSmallIcon(contentIconRes)
            .setContentTitle(contentTitle)
            .setContentText(contentText)
//            .setStyle(NotificationCompat.BigTextStyle().bigText("BigTextStyle")) // TODO: try different style
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        with(notificationManager) {
            notify(notificationId, builder.build())
        }
    }

    fun cancelAllNotification() {
        notificationManager.cancelAll()
    }

    fun handleNotification(activity: AppCompatActivity) {
        val intent = activity.intent ?: return
        if (isFromNotification(intent) && intent.hasExtra(EXTRA_NOTIFICATION_CONTENT_TEXT)) {
            val contentText = intent.getStringExtra(EXTRA_NOTIFICATION_CONTENT_TEXT)

            // remove content to prevent duplicate handling
            intent.removeExtra(EXTRA_NOTIFICATION_CONTENT_TEXT)

            contentText?.let {
                showNotificationResultBottomSheet(activity, it)
            }
        }
    }

    private fun showNotificationResultBottomSheet(activity: AppCompatActivity, contentText: String) {
        val bottomSheetDialog = BottomSheetDialog(activityContext)

        val bottomSheetView = activity.layoutInflater.inflate(R.layout.view_bottom_sheet, null)
        bottomSheetView.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        val contentTextView = bottomSheetView.findViewById<TextView>(R.id.bottom_sheet_text)
        contentTextView.text = contentText

        val button = bottomSheetView.findViewById<TextView>(R.id.bottom_sheet_button)
        if (URLUtil.isValidUrl(contentText)) {
            button.visibility = View.VISIBLE
            button.setOnClickListener {
                openCustomBrowser(activity, contentText)
            }
        } else {
            button.visibility = View.GONE
        }

        bottomSheetDialog.setContentView(bottomSheetView)
        bottomSheetDialog.show()
    }

    companion object {
        const val EXTRA_NOTIFICATION_CONTENT_TEXT = "EXTRA_NOTIFICATION_CONTENT_TEXT"
        const val DEFAULT_CHANNEL_ID = "default_notification_channel"

        fun createUniqueId(): Int {
            return parseInt(SimpleDateFormat("ddHHmmss", Locale.CANADA).format(Date()))
        }

        fun isFromNotification(intent: Intent?): Boolean {
            if (intent == null) {
                return false
            }
            // check intent is from notification
            // https://stackoverflow.com/questions/27123027/testing-that-an-activity-has-been-started-with-flag-activity-clear-top/27168803
            val flag = Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY
            return intent.flags != flag && (intent.flags and flag) == 0
        }
    }
}