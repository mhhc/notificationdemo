package app.demo.notification

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import app.demo.notification.databinding.ActivityMainBinding
import app.demo.notification.util.NotificationHelper
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var notificationHelper: NotificationHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        notificationHelper = NotificationHelper(this)
        binding.mainTitle.append(" - v${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})")
        binding.mainLocalNotificationContentForm.sendNotificationButton.setOnClickListener {
            notificationHelper.sendNotification(
                getSelectedNotificationIcon(),
                binding.mainLocalNotificationContentForm.notificationTitleInputText.text.toString(),
                binding.mainLocalNotificationContentForm.notificationTextInputText.text.toString(),
            )
        }
        binding.mainLocalNotificationContentForm.cancelAllNotificationButton.setOnClickListener {
            notificationHelper.cancelAllNotification()
        }
        binding.mainCredit.setOnClickListener {
            startActivity(Intent(this, OssLicensesMenuActivity::class.java))
        }
    }

    private fun getSelectedNotificationIcon(): Int {
        when (binding.mainLocalNotificationContentForm.notificationToggleGroup.checkedButtonId) {
            R.id.notification_icon_default -> {
                return R.drawable.ic_notifications_active
            }
            R.id.notification_icon_send -> {
                return R.drawable.ic_send_notification
            }
            R.id.notification_icon_scan -> {
                return R.drawable.ic_scan
            }
            R.id.notification_icon_flash -> {
                return R.drawable.ic_flash_on
            }
            else -> {
                return R.drawable.ic_notifications_active
            }
        }
    }

    override fun onResume() {
        super.onResume()
        // TODO: handle notification from another activity https://developer.android.com/training/notify-user/navigation
        notificationHelper.handleNotification(this)
    }

}