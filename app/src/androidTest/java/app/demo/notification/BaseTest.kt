package app.demo.notification

import android.content.Context
import android.os.SystemClock
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject2
import androidx.test.uiautomator.Until
import org.junit.Assert
import org.junit.Before
import org.junit.Rule

open class BaseTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun beforeTestCheck() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertTrue("beforeTestCheck packageName is changed.", appContext.packageName.startsWith("app.demo.notification"))
    }

    companion object {
        private val appContext:Context = ApplicationProvider.getApplicationContext()
        private val uiDevice: UiDevice = UiDevice.getInstance(getInstrumentation())

        fun getString(resId: Int): String {
            return appContext.resources.getString(resId)
        }

        fun wait(seconds: Float) = apply {
            SystemClock.sleep(seconds.toLong() * 1000L)
        }

        // https://alexzh.com/guide-testing-android-notifications/
        fun assertNotificationDisplayed(
            title: String,
            text: String,
            clickOnNotification: Boolean
        ) {
            uiDevice.openNotification()
            uiDevice.wait(Until.hasObject(By.textStartsWith(getString(R.string.app_name))), 3000L)

            val titleObject: UiObject2 = uiDevice.findObject((By.textStartsWith(title)))
            if (!title.isNullOrBlank()) {
                Assert.assertTrue(titleObject.text.equals(title))
            }

            val textObject: UiObject2 = uiDevice.findObject((By.textStartsWith(text)))
            if (!text.isNullOrBlank()) {
                Assert.assertTrue(textObject.text.equals(text))
            }

            if (clickOnNotification) {
                if (!title.isNullOrBlank()) {
                    titleObject.click()
                } else if (!text.isNullOrBlank()) {
                    textObject.click()
                }
            }
        }
    }
}