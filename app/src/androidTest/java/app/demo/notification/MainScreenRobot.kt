package app.demo.notification

import androidx.test.espresso.matcher.ViewMatchers.*

// https://medium.com/android-bits/espresso-robot-pattern-in-kotlin-fc820ce250f7
// By using the power of Kotlin we make each fun return the object itself with apply.
fun mainScreen(robotMethod: MainScreenRobot.() -> Unit) =
    MainScreenRobot().apply { robotMethod() }

class MainScreenRobot : BaseTestRobot() {

    fun selectNotificationIcon(iconResId: Int) {
        clickView(withId(iconResId))
    }

    fun inputNotificationTitle(title: String) {
        inputTextToView(withId(R.id.notification_title_input_text), title)
    }

    fun inputNotificationText(text: String) {
        inputTextToView(withId(R.id.notification_text_input_text), text)
    }

    fun clickSendNotificationButton() {
        clickView(withId(R.id.send_notification_button))
    }

    fun assertNotificationResult(text: String) {
        checkViewIsVisible(withId(R.id.bottom_sheet_icon))
        checkViewIsVisibleWithText(withId(R.id.bottom_sheet_label), withText(R.string.notification_text_received))
        checkViewIsVisibleWithText(withId(R.id.bottom_sheet_text), withSubstring(text))
    }

}