package app.demo.notification

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import org.hamcrest.Matcher
import java.util.concurrent.TimeoutException

open class BaseTestRobot {

    // TODO: improve - this seems like a hack
    fun ViewInteraction.wait(seconds: Float): ViewInteraction {
        BaseTest.wait(1.0F)
        val start = System.currentTimeMillis()
        val end = start + (seconds * 1000L)
        do {
            try {
                check(matches(isDisplayed()))
                return this
            } catch (e: Exception) {
                BaseTest.wait(1.0F)
            }
        } while (System.currentTimeMillis() < end)

        throw TimeoutException("View is not found.")
    }

    fun checkViewIsVisible(matcher: Matcher<View>): ViewInteraction =
        onView(matcher)
            .wait(3.0F)
            .check(matches(isCompletelyDisplayed()))
            .check(matches(isDisplayed()))

    fun checkViewIsVisibleWithText(viewMatcher: Matcher<View>, textMatcher: Matcher<View>): ViewInteraction =
        onView(viewMatcher)
            .wait(3.0F)
            .check(matches(isCompletelyDisplayed()))
            .check(matches(textMatcher))
            .check(matches(isDisplayed()))

    fun clickView(matcher: Matcher<View>): ViewInteraction =
        onView(matcher)
            .wait(3.0F)
            .check(matches(isCompletelyDisplayed()))
            .perform(click())

    fun inputTextToView(matcher: Matcher<View>, text: String): ViewInteraction =
        onView(matcher)
            .wait(3.0F)
            .check(matches(isCompletelyDisplayed()))
            .perform(
                ViewActions.replaceText(text),
                ViewActions.pressImeActionButton()
            )
}