package app.demo.notification

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainScreenTest : BaseTest() {

    @Test
    fun checkNotification() {
        val iconId = R.id.notification_icon_flash
        val title = getString(R.string.notification_title)
        val text = getString(R.string.notification_text)

        mainScreen {
            selectNotificationIcon(iconId)
            inputNotificationTitle(title)
            inputNotificationText(text)
            clickSendNotificationButton()
        }

        assertNotificationDisplayed(title, text, true)

        mainScreen {
            assertNotificationResult(text)
        }
    }

}