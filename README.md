# Notification Demo App #
This is a demo app for self-learning on Android Jetpack, notification, and Espresso Robot Pattern.

![App Icon](https://play-lh.googleusercontent.com/HBYEZFugaUkcFeTcOhEbEBPmVFK-C5m0oE95BC1lKkv1nXN70jUNptHDVutHnZb5Gw=s180-rw)

![App Screenshot1](https://lh3.googleusercontent.com/cKTj9CFeufEobnh9wwVbmDiOxTfYeg0TNNEeeIVS-WBVkhCWBkjDKFsCd32pqBC4duQ)
![App Screenshot2](https://lh3.googleusercontent.com/lnt5xDP1tJzbYxp0iEp-ES3L78hup8eTz54Fehz9F1ZP5QJCYFeFX-FVtL_q6IsFc-g)
![App Screenshot3](https://lh3.googleusercontent.com/x6yXkm3M3hV3cal4ngJ5ysSsJlwXkJUGAMG1M-mbGf9vhgRtbnuP0x-cXFm0pRqJ3g)

![App Screenshot4](https://lh3.googleusercontent.com/qMdQu2-y5JM0hFOdE5mmD2O-CmRx9tkOT1u6ie_VDtnEM0PKZQW5kdIpM2fGYnsu6Q)
![App Screenshot5](https://lh3.googleusercontent.com/BBQbjDkV1QtYEgvln7SE1cVcLICEAXn1Hsx-XKGeF6LhANExJRi0cdjaltSpqZJrO48)
![App Screenshot6](https://lh3.googleusercontent.com/NU4nkHxGpD2kuS0pe1v_7AnXEV7FnlS5dey3ktPeUs3thUFgvU8xCHeu7JtdEkUvhOI)

### Version History ###

#### Version 1.0.0 (1) - 2021.02.15 - It sends a local notification for user to see what it looks like. User may open the URL in a CustomTab. ####

* Android notification
    * build and send notification
        * https://developer.android.com/training/notify-user/build-notification
    * handle intent from notification
        * https://developer.android.com/training/notify-user/navigation
        * https://stackoverflow.com/questions/27123027/testing-that-an-activity-has-been-started-with-flag-activity-clear-top/27168803
    
* include Android Jetpack libraries
    * [integrate View Binding](https://developer.android.com/topic/libraries/view-binding)
        * "In most cases, view binding replaces findViewById."
    * include AndroidX Layouts / Material Design Components
        * ConstraintLayout https://developer.android.com/training/constraint-layout
            * build responsive UI with ConstraintLayout: chain https://developer.android.com/training/constraint-layout#constrain-chain
        * CoordinatorLayout https://developer.android.com/reference/kotlin/androidx/coordinatorlayout/widget/CoordinatorLayout
        * TextInputLayout https://material.io/components/text-fields/android#using-text-fields
        * MaterialButton https://material.io/components/buttons/android#text-button
        * Dark theme https://developer.android.com/guide/topics/ui/look-and-feel/darktheme
        * MaterialButtonToggleGroup https://developer.android.com/reference/com/google/android/material/button/MaterialButtonToggleGroup
            * https://www.journaldev.com/31950/android-materialbuttontogglegroup
            * https://learntodroid.com/how-to-create-a-button-group-with-materialbuttontogglegroup/
        * TextInputEditText https://material.io/components/text-fields/android
            * https://www.journaldev.com/14748/android-textinputlayout-example
        * BottomSheetDialog https://material.io/components/sheets-bottom#usage
            * https://wise4rmgodadmob.medium.com/a-simple-example-of-using-bottomsheetdialog-in-android-development-using-kotlin-152be3b29737
            * https://medium.com/glucosio-project/moving-from-dialogs-to-bottomsheetdialogs-on-android-15fb8d140295
    * improve layout performance https://developer.android.com/training/improving-layouts/reusing-layouts

* include Android Web: [Custom Tabs](https://developers.google.com/web/android/custom-tabs/implementation-guide) 
    * opens URL in an internal Chrome tab in the app (so it does not open the Chrome browser app)

* include [Open Source Notices](https://developers.google.com/android/guides/opensource)

#### Version 1.0.0 (2) - 2021.02.19 - Espresso Robot Pattern ####

* Espresso https://developer.android.com/training/testing/espresso
    * Robot Pattern https://medium.com/android-bits/espresso-robot-pattern-in-kotlin-fc820ce250f7
    * test notification https://alexzh.com/guide-testing-android-notifications/


### Future Learning ###

* Firebase cloud messaging https://firebase.google.com/docs/cloud-messaging
    * NOTE: Privacy Policy for Push Notifications https://www.termsfeed.com/blog/privacy-policy-push-notifications/
    

### Reference ###

* best practice
    * Kotlin coding convention https://kotlinlang.org/docs/coding-conventions.html#class-layout
* https://developer.android.com/jetpack
* notification sample
    * https://github.com/android/user-interface-samples/tree/main/Notifications
    * https://github.com/googlearchive/android-NotificationChannels/blob/master/Application/src/main/java/com/example/android/notificationchannels/NotificationHelper.java
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)